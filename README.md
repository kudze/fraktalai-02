# Riedantis skritulys

Ši programa, naudodama matricų operacijas, suanimuoja aspkritimą, kuris iš pozicijos (0, 0),
nurieda į poziciją (4, 0). Po to, parkrenta ant šono.

## Priklausomybės

1) PHP 8.3
2) Composer
3) PHP-GD pletinys

## Paleidimas

1. `composer install`, atsiunčia naudojamas php bibliotekas (Gif generavimui)
2. `php skritulys.php 60 15 30`, sugeneruoja out.gif