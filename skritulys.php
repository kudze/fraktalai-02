<?php

use NumPHP\Core\NumArray;
use Intervention\Gif\Builder;

require_once __DIR__ . "/vendor/autoload.php";

const W = 512;
const H = 512;

const X_MAX = 6;
const Y_MAX = 6;

const X_OFFSET = -1;
const Y_OFFSET = -1;

const FONT = "/usr/share/fonts/truetype/liberation2/LiberationSans-Regular.ttf";

function calcVectorLength(NumArray $vec): float
{
    [$x, $y] = $vec->getData();

    return sqrt($x * $x + $y * $y);
}

function fromDotToScreen(NumArray $dot): NumArray
{
    $xScale = W / (float)X_MAX;
    $yScale = H / (float)Y_MAX;

    return scaleXY($dot->sub(new NumArray([X_OFFSET, Y_OFFSET + Y_MAX])), $xScale, -$yScale);
}

/**
 * @throws Exception
 */
function drawFrame(string $outputFile, array $circlePos, NumArray $pos)
{
    $image = imagecreatetruecolor(W, H);
    if ($image === FALSE)
        throw new Exception("Failed to create an image");

    $bgColor = imagecolorallocate($image, 255, 255, 255);
    imagefill($image, 0, 0, $bgColor);

    $col1 = imagecolorallocate($image, 255, 0, 0);
    $col2 = imagecolorallocate($image, 0, 255, 0);
    $col3 = imagecolorallocate($image, 0, 0, 255);
    $col4 = imagecolorallocate($image, 255, 255, 0);
    $colText = imagecolorallocate($image, 0, 0, 0);

    [$x, $y] = $pos->getData();
    $xStr = number_format($x, 2);
    $yStr = number_format($y, 2);
    [$x, $y] = fromDotToScreen($pos)->getData();

    imagefilledpolygon($image, array_merge(...array_map(fn(NumArray $point) => fromDotToScreen($point)->getData(), $circlePos['red'])), count($circlePos['red']), $col1);
    imagefilledpolygon($image, array_merge(...array_map(fn(NumArray $point) => fromDotToScreen($point)->getData(), $circlePos['green'])), count($circlePos['green']), $col2);
    imagefilledpolygon($image, array_merge(...array_map(fn(NumArray $point) => fromDotToScreen($point)->getData(), $circlePos['blue'])), count($circlePos['blue']), $col3);
    imagefilledpolygon($image, array_merge(...array_map(fn(NumArray $point) => fromDotToScreen($point)->getData(), $circlePos['yellow'])), count($circlePos['yellow']), $col4);
    imagettftext($image, 10, 0, $x, $y, $colText, FONT, "{{$xStr}, {$yStr}}");

    imagegif($image, $outputFile);
    imagedestroy($image);
}

function scaleXY(NumArray $pos, float $scaleX, float $scaleY): NumArray
{
    $pos = new NumArray($pos->getData());
    $scaleMatrix = new NumArray([[$scaleX, 0], [0, $scaleY]]);

    return $scaleMatrix->dot($pos);
}

function rotateAndScaleY(NumArray $pos, float $rotRad, float $scaleY, NumArray $centerOfCircle): NumArray
{
    $pos = new NumArray($pos->getData());
    $cos = cos($rotRad);
    $sin = sin($rotRad);

    $rotationMatrix = new NumArray([
        [$cos, $sin],
        [-$sin, $cos]
    ]);

    return (scaleXY($rotationMatrix->dot($pos->sub($centerOfCircle)), 1, $scaleY))->add($centerOfCircle);
}

if ($argc !== 4) {
    echo "Naudojimas: php skritulys.php <framesRotate> <framesFlip> <fps>\n";
    return;
}

function removeTempOutput()
{
    global $OUT_DIR;
    array_map('unlink', glob($OUT_DIR . "/*.*"));
    rmdir($OUT_DIR);
}

$FRAMES_ROTATE = (int)$argv[1];
$FRAMES_FLIP = (int)$argv[2];
$FPS = (int)$argv[3];

$TOTAL_FRAMES = $FRAMES_ROTATE + $FRAMES_FLIP;

$OUT_DIR = __DIR__ . "/out";
if (is_dir($OUT_DIR))
    removeTempOutput();

mkdir($OUT_DIR);

$circleDots = [
    'red' => [
        new NumArray([0, 1]),
        new NumArray([1, 1]),
        new NumArray([0.98, 0.8]),
        new NumArray([0.8, 0.4]),
        new NumArray([0.4, 0.0834]),
        new NumArray([0, 0]),
    ],
    'green' => [
        new NumArray([0, 1]),
        new NumArray([-1, 1]),
        new NumArray([-0.98, 0.8]),
        new NumArray([-0.8, 0.4]),
        new NumArray([-0.4, 0.0834]),
        new NumArray([0, 0]),
    ],
    'blue' => [
        new NumArray([0, 1]),
        new NumArray([-1, 1]),
        new NumArray([-0.98, 1.2]),
        new NumArray([-0.8, 1.6]),
        new NumArray([-0.4, 1.9166]),
        new NumArray([0, 2]),
    ],
    'yellow' => [
        new NumArray([0, 1]),
        new NumArray([1, 1]),
        new NumArray([0.98, 1.2]),
        new NumArray([0.8, 1.6]),
        new NumArray([0.4, 1.9166]),
        new NumArray([0, 2]),
    ],
];
$startCenterPos = new NumArray([0, 1]);

$totalMoveDistance = new NumArray([4, 0]);
$totalMoveDistanceLength = calcVectorLength($totalMoveDistance);
$totalRotateDeg = ($totalMoveDistanceLength * 360) / (2 * pi());
$totalMoveDistanceForFlip = new NumArray([0, -1]);

$files = [];
foreach (range(0, $TOTAL_FRAMES - 1) as $frame) {
    $totalRotateProgress = min(1., $frame / ((float)$FRAMES_ROTATE - 1));
    $totalFlipProgress = max(0., ($frame - $FRAMES_ROTATE + 1) / ((float)$FRAMES_FLIP));

    if ($totalRotateProgress === 0.) {
        $currMoveDistance = new NumArray([0, 0]);
        $currRotateDeg = 0;
    } else {
        $currMoveDistance = (new NumArray($totalMoveDistance->getData()))->mult($totalRotateProgress);
        $currRotateDeg = $totalRotateDeg * $totalRotateProgress;
    }

    if ($totalFlipProgress === 0)
        $currScaleY = 1;
    else {
        $currScaleY = (1 - 1 * $totalFlipProgress) + 0.001; //Add rly small y so that we dont divide by zero.
        $currMoveDistanceForFlip = (new NumArray($totalMoveDistanceForFlip->getData()))->mult($totalFlipProgress);
        $currMoveDistance->add($currMoveDistanceForFlip);
    }

    $currCenterPos = rotateAndScaleY($startCenterPos, deg2rad($currRotateDeg), $currScaleY, $startCenterPos)->add($currMoveDistance);
    $currCircleDots = array_map(
        fn(array $group) => array_map(
            fn(NumArray $point) => rotateAndScaleY($point, deg2rad($currRotateDeg), $currScaleY, $startCenterPos)->add($currMoveDistance),
            $group
        ),
        $circleDots
    );

    $file = "$OUT_DIR/test$frame.png";
    $files[] = $file;
    drawFrame($file, $currCircleDots, $currCenterPos);
}

$gif = Builder::canvas(W, H, 0);
foreach ($files as $file)
    $gif->addFrame($file, 1. / $FPS);

file_put_contents('out.gif', $gif->encode());
removeTempOutput();